from DataBase.MongoDB import mng
class Agenda(mng):

    def __init__(self,conexion_mng):
        self.con_mng=conexion_mng
    def insertarContacto(self,contacto):
        nombre=contacto['nombre']
        apellido=contacto['apellido']
        telefono_cell=contacto['telefono_cell']
        telefono_fijo=contacto['telefono_fijo']
        email=contacto['email']
        idd=contacto["id"]
        contacto={"Nombre":nombre,"Apellido":apellido,"Telf_cell":telefono_cell,"Telf_fijo":telefono_fijo,"Email":email,"id":idd}
        self.con_mng.mycol.insert_one(contacto)
    def listarContacto(self):
        lst=[]
        for x in self.con_mng.mycol.find():
            #print(x)
            lst.append(x)
        return lst
    def buscarContacto(self,nombre):
        myquery = { "Nombre": nombre }
        mydoc = self.con_mng.mycol.find(myquery)
        contacto_buscado={}
        print("buscar contacto")
        for x in mydoc:
            print(x)
            contacto_buscado=x
        return contacto_buscado
    def editarContacto(self,nombre_old,contacto):
        myquery = { "Nombre": nombre_old }

        nombre=contacto['nombre']
        apellido=contacto['apellido']
        telefono_cell=contacto['telefono_cell']
        telefono_fijo=contacto['telefono_fijo']
        email=contacto['email']
        idd=contacto["id"]
        contacto={"Nombre":nombre,"Apellido":apellido,"Telf_cell":telefono_cell,"Telf_fijo":telefono_fijo,"Email":email,"id":idd}
        newvalues = { "$set": contacto }
        self.con_mng.mycol.update_one(myquery,newvalues)
    def eliminarContacto(self,contacto_eliminar):
        myquery = { "Nombre": contacto_eliminar}
        self.con_mng.mycol.delete_one(myquery)
    def editarContactoId(self,idd,contacto):
        myquery = { "id": idd}

        nombre=contacto['nombre']
        apellido=contacto['apellido']
        telefono_cell=contacto['telefono_cell']
        telefono_fijo=contacto['telefono_fijo']
        email=contacto['email']
        idd=contacto["id"]
        contacto={"Nombre":nombre,"Apellido":apellido,"Telf_cell":telefono_cell,"Telf_fijo":telefono_fijo,"Email":email,"id":idd}
        newvalues = { "$set": contacto }
        self.con_mng.mycol.update_one(myquery,newvalues)
    def eliminarContactoId(self,id_eliminar):
        myquery = { "id": id_eliminar}
        self.con_mng.mycol.delete_one(myquery)
    def buscarContactoId(self,idd):
        myquery = { "id": idd }
        mydoc = self.con_mng.mycol.find(myquery)
        contacto_buscado={}
        for x in mydoc:
            print(x)
            contacto_buscado=x
        return contacto_buscado
