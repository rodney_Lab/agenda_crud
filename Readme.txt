# CRUD AGENDA
Se debe almacenar para cada contacto los siguiente parametros: nombres, apellido paterno, apellido maternos, movil (02 números), email.

- Añadir contacto
- Buscar contacto
- Editar contacto
- Eliminar contacto
- Listar contactos

## Restricciones

- Usar programación orientada a objetos. (Clases)
- Usar la libreria Flask y Flask_RestX (Resource)
- Usar Postman o Swagger para documentar.
- Mostrar archivo requirements, con las librerias a usar.
- Crear Readme para levantar servicio y detallar para que sirve la aplicación.
- Usar tags por cada punto realizado. (GIT)
- Commits con una correcta descripción.