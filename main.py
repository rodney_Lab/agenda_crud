from DataBase.MongoDB import mng
from Agenda import Agenda

from flask import Flask, request, jsonify
from flask_restx import Resource, Api

import flask

app = Flask(__name__)
api = Api(app)

#Conexion mongo
mn=mng()

#Clase agenda
ag=Agenda(mn)

@api.route('/hello')
class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}

@app.route('/Agenda_Add', methods=['POST'])
def AgregarContacto():
    content = request.json
    contacto=content['Contacto']
    print(contacto['nombre'])
    print(contacto['apellido'])
    print(contacto['telefono_cell'])
    print(contacto['telefono_fijo'])
    print(contacto['email'])
    ag.insertarContacto(contacto)
    return "ok",200
@app.route('/Agenda_List', methods=['GET'])
def ListarContacto():

    contactos_DB=ag.listarContacto()
    contactos=[]

    for i in contactos_DB:
        con={
            "nombre":i['Nombre'],
            "apellido":i['Apellido'],
            "telefono_cell":i['Telf_cell'],
 		    "telefono_fijo":i['Telf_fijo'],
 		    "email":i['Email']
        }
        contactos.append(con)

    data={
        "data":contactos
    }

    return jsonify(data)

@app.route('/Agenda_Edit', methods=['PUT'])
def EditarContacto():
    content = request.json
    #Buscar Contacto a editar
    nombre=content['Contacto']['nombre']
    nuevo=content['NewData']
    ag.editarContacto(nombre,nuevo)
    return "ok",200

@app.route('/Agenda_Del', methods=['DELETE'])
def EliminarContacto():
    content = request.json
    #Buscar Contacto a Eliminar
    nombre=content['Contacto']['nombre']
    ag.eliminarContacto(nombre)
    return "ok",200
@app.route('/Agenda_Search', methods=['GET'])
def BuscarContacto():
    content = request.json
    #Buscar Contacto
    nombre=content['Contacto']['nombre']
    contacto_buscado=ag.buscarContacto(nombre)
    data={
         "Contacto":{
 		"nombre":contacto_buscado['Nombre'],
 		"apellido":contacto_buscado['Apellido'],
 		"telefono_cell":contacto_buscado['Telf_cell'],
 		"telefono_fijo":contacto_buscado['Telf_fijo'],
 		"email":contacto_buscado['Email']
        }
    }
    return jsonify(data)

#Buscar , editar y eliminar contactos según el Id del contacto
@app.route('/Agenda_Id', methods=['GET','PUT','DELETE'])
def TestParametro():
    
    content = request.json
    idd = request.args.get('id', default = 0, type = int)
    print("Id contacto : "+str(idd))

    if flask.request.method == 'GET':
        contacto_buscado=ag.buscarContactoId(idd)
        data={
            "Contacto":{
 		    "nombre":contacto_buscado['Nombre'],
 		    "apellido":contacto_buscado['Apellido'],
 		    "telefono_cell":contacto_buscado['Telf_cell'],
 		    "telefono_fijo":contacto_buscado['Telf_fijo'],
 		    "email":contacto_buscado['Email'],
             "id":contacto_buscado['id']
            }
        }
        print("Buscar Contacto")
        return jsonify(data)
    if flask.request.method == 'DELETE':
        ag.eliminarContactoId(idd)
        print("Eliminar contacto")
    if flask.request.method == 'PUT':
        nuevo=content['NewData']
        ag.editarContactoId(idd,nuevo)
        print("Editar contacto")

    return "ok",200
if __name__ == '__main__':
    app.run(debug=True)


